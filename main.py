# Writer: LauNT #Date: 20/03/2024

from fastapi import FastAPI

import uvicorn
import socket


app = FastAPI()

@app.get("/")
def read_root():
    hostname = socket.gethostname()
    return {"hostname": hostname}


if __name__ == "__main__":
    uvicorn.run(
        "main:app", 
        host="0.0.0.0", 
        port=6868
    )